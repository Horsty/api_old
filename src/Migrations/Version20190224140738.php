<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190224140738 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE card (id VARCHAR(255) NOT NULL, name VARCHAR(100) NOT NULL, description LONGTEXT DEFAULT NULL, base_id INT DEFAULT NULL, awoken_id INT DEFAULT NULL, dokkan_id INT DEFAULT NULL, dokkan_awakable INT DEFAULT NULL, potential_board_id INT DEFAULT NULL, character_name VARCHAR(255) DEFAULT NULL, character_id VARCHAR(255) DEFAULT NULL, unique_info_id INT DEFAULT NULL, card_name VARCHAR(255) DEFAULT NULL, resource_id INT DEFAULT NULL, card_type INT DEFAULT NULL, card_subtype INT DEFAULT NULL, base_hp_init INT DEFAULT NULL, base_hp_max INT DEFAULT NULL, base_atk_init INT DEFAULT NULL, base_atk_max INT DEFAULT NULL, base_def_init INT DEFAULT NULL, base_def_max INT DEFAULT NULL, base_avg_init INT DEFAULT NULL, base_avg_max INT DEFAULT NULL, awoken_hp_init INT DEFAULT NULL, awoken_hp_max INT DEFAULT NULL, awoken_atk_init INT DEFAULT NULL, awoken_atk_max INT DEFAULT NULL, awoken_def_init INT DEFAULT NULL, awoken_def_max INT DEFAULT NULL, awoken_avg_init INT DEFAULT NULL, awoken_avg_max INT DEFAULT NULL, pot_hp_max INT DEFAULT NULL, pot_atk_max INT DEFAULT NULL, pot_def_max INT DEFAULT NULL, pot_avg_max INT DEFAULT NULL, base_open_at INT DEFAULT NULL, base_rarity INT DEFAULT NULL, base_element INT DEFAULT NULL, base_cost INT DEFAULT NULL, base_lv_max INT DEFAULT NULL, base_skill_lv_max INT DEFAULT NULL, base_grow_type INT DEFAULT NULL, base_exp_type INT DEFAULT NULL, base_price INT DEFAULT NULL, base_training_exp INT DEFAULT NULL, base_eball_min INT DEFAULT NULL, base_eball_num100 INT DEFAULT NULL, base_eball_max INT DEFAULT NULL, base_eball_max_num INT DEFAULT NULL, base_selling_exchange_point INT DEFAULT NULL, awoken_open_at INT DEFAULT NULL, awoken_rarity INT DEFAULT NULL, awoken_element INT DEFAULT NULL, awoken_cost INT DEFAULT NULL, awoken_lv_max INT DEFAULT NULL, awoken_skill_lv_max INT DEFAULT NULL, awoken_grow_type INT DEFAULT NULL, awoken_exp_type INT DEFAULT NULL, awoken_price INT DEFAULT NULL, awoken_training_exp INT DEFAULT NULL, awoken_eball_min INT DEFAULT NULL, awoken_eball_num100 INT DEFAULT NULL, awoken_eball_max INT DEFAULT NULL, awoken_eball_max_num INT DEFAULT NULL, awoken_selling_exchange_point INT DEFAULT NULL, pot_skill_lv_max INT DEFAULT NULL, leader_id INT DEFAULT NULL, leader_name VARCHAR(255) DEFAULT NULL, leader_description LONGTEXT DEFAULT NULL, passive_skill_set_id INT DEFAULT NULL, base_awakening_set_id INT DEFAULT NULL, dokkan_awakening_set_id INT DEFAULT NULL, rarity VARCHAR(255) DEFAULT NULL, special VARCHAR(255) DEFAULT NULL, special_set JSON DEFAULT NULL, special_detail VARCHAR(255) DEFAULT NULL, passive_name VARCHAR(255) DEFAULT NULL, passive_description LONGTEXT DEFAULT NULL, display_value INT DEFAULT NULL, base_awakening_medals LONGTEXT DEFAULT NULL, dokkan_awakening_medals LONGTEXT DEFAULT NULL, dokkan_zeni VARCHAR(255) DEFAULT NULL, effect TINYINT(1) DEFAULT NULL, imgur_thumb VARCHAR(255) DEFAULT NULL, uri VARCHAR(255) DEFAULT NULL, thumb LONGTEXT DEFAULT NULL, hash VARCHAR(255) DEFAULT NULL, dokkan_count INT DEFAULT NULL, eza VARCHAR(255) DEFAULT NULL, extreme_z TINYINT(1) DEFAULT NULL, reverse_thumb LONGTEXT DEFAULT NULL, assets LONGTEXT DEFAULT NULL, dokkan_thumb LONGTEXT DEFAULT NULL, s3_base VARCHAR(255) DEFAULT NULL, dataset VARCHAR(255) DEFAULT NULL, ticks LONGTEXT DEFAULT NULL, links_html LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE card_category (card_id VARCHAR(255) NOT NULL, category_id VARCHAR(255) NOT NULL, INDEX IDX_BF7DD7424ACC9A20 (card_id), INDEX IDX_BF7DD74212469DE2 (category_id), PRIMARY KEY(card_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE card_link (card_id VARCHAR(255) NOT NULL, link_id VARCHAR(255) NOT NULL, INDEX IDX_DA9F9B534ACC9A20 (card_id), INDEX IDX_DA9F9B53ADA40271 (link_id), PRIMARY KEY(card_id, link_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, import_id INT NOT NULL, UNIQUE INDEX UNIQ_64C19C1B6A263D9 (import_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE link (id VARCHAR(255) NOT NULL, import_id INT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, uri VARCHAR(255) DEFAULT NULL, efficacy_type INT DEFAULT NULL, target_type INT DEFAULT NULL, influence_type INT DEFAULT NULL, unique_characters VARCHAR(255) DEFAULT NULL, card_count VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_36AC99F1B6A263D9 (import_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id VARCHAR(255) NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, api_token VARCHAR(255) NOT NULL, is_active TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D6497BA2F5EB (api_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE special_set (id INT AUTO_INCREMENT NOT NULL, eball_num_start INT DEFAULT NULL, special_name VARCHAR(255) DEFAULT NULL, special_description LONGTEXT DEFAULT NULL, aim_target INT DEFAULT NULL, increase_rate INT DEFAULT NULL, lv_init INT DEFAULT NULL, lv_max INT DEFAULT NULL, lv_bonus INT DEFAULT NULL, special_bonus_lv1 INT DEFAULT NULL, sb1name VARCHAR(255) DEFAULT NULL, sb1description VARCHAR(255) DEFAULT NULL, special_bonus_lv2 INT DEFAULT NULL, sb2name VARCHAR(255) DEFAULT NULL, sb2description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE card_category ADD CONSTRAINT FK_BF7DD7424ACC9A20 FOREIGN KEY (card_id) REFERENCES card (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE card_category ADD CONSTRAINT FK_BF7DD74212469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE card_link ADD CONSTRAINT FK_DA9F9B534ACC9A20 FOREIGN KEY (card_id) REFERENCES card (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE card_link ADD CONSTRAINT FK_DA9F9B53ADA40271 FOREIGN KEY (link_id) REFERENCES link (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE card_category DROP FOREIGN KEY FK_BF7DD7424ACC9A20');
        $this->addSql('ALTER TABLE card_link DROP FOREIGN KEY FK_DA9F9B534ACC9A20');
        $this->addSql('ALTER TABLE card_category DROP FOREIGN KEY FK_BF7DD74212469DE2');
        $this->addSql('ALTER TABLE card_link DROP FOREIGN KEY FK_DA9F9B53ADA40271');
        $this->addSql('DROP TABLE card');
        $this->addSql('DROP TABLE card_category');
        $this->addSql('DROP TABLE card_link');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE link');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE special_set');
    }
}
