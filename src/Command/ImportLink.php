<?php
/**
 * Created by PhpStorm.
 * User: cyril
 * Date: 2019-02-17
 * Time: 17:22
 */

namespace App\Command;

use App\Controller\LinkController;
use App\Entity\Link;
use App\Repository\LinkRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;


class ImportLink extends Command
{

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'dokkan:import-link';
    private $em;
    private $linkController;

    public function __construct(EntityManagerInterface $entityManager, LinkController $linkController)
    {
        $this->em = $entityManager;
        $this->linkController = $linkController;
        parent::__construct();
    }

    protected function configure()
    {
        // ...
        $this->addOption(
            'name',
            null,
            InputOption::VALUE_OPTIONAL,
            'Name of files.',
            'links'
        )

            ->setDescription('Import link from a json to sql')
            ->setHelp('This command allows you to import link...')
        ;

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // ...
        // Showing when the script is launched
        $now = new \DateTime();
        $output->writeln('<comment>Start : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');

        // Importing JSON on DB via Doctrine ORM
        $this->import($input, $output);

        // Showing when the script is over
        $now = new \DateTime();
        $output->writeln(PHP_EOL . '<comment>End : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
    }

    protected function import(InputInterface $input, OutputInterface $output)
    {
        // Getting php array of data from Json
        $data = $this->get($input, $output);

        // Getting doctrine manager
        //$em = $this->getDoctrine()->getManager();

        // Turning off doctrine default logs queries for saving memory
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        //$em->getConnection()->getConfiguration()->setSQLLogger(null);

        // Define the size of record, the frequency for persisting the data and the current index of records
        $size = count($data);
        $batchSize = 20;
        $i = 1;
        // Starting progress
        $progress = new ProgressBar($output, $size);
        $progress->start();

        // Processing on each row of data
        foreach ($data as $index => $object) {
            if ($object) {
                $link = new Link();
                //$decoded = json_decode($object);

                $form = $this->linkController->getForm($link);
                $form->submit($object);
                $link->setImportId($object['id']);

                $this->em->persist($link);

                // Each 20 cards persisted we flush everything
                if (($i % $batchSize) === 0) {
                    $this->em->flush();
                    // Detaches all objects from Doctrine for memory save
                    $this->em->clear();
                    $progress->advance($batchSize);
                    $now = new \DateTime();
                    $output->writeln(' of links imported ... | ' . $now->format('d-m-Y G:i:s'));
                }
            }

            $i++;
        }
        // Flushing and clear data on queue
        $this->em->flush();
        $this->em->clear();
        // Ending the progress bar process
        $progress->finish();
    }

    protected function get(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getOption('name');
        // Getting the CSV from filesystem
        $fileName = 'src/ressources/dokkan/'. $name .'.json';

        // Using service for converting CSV to PHP Array
        //$converter = $this->getContainer()->get('import.csvtoarray');
        $data = $this->convert($fileName);

        return $data;
    }

    /**
     * @TODO créer un repository / service pour faire cette méthode
     * @param $filename
     * @return bool|mixed
     */
    public function convert($filename)
    {
        if(!file_exists($filename) || !is_readable($filename)) {
            return FALSE;
        }

        $string = file_get_contents($filename);
        $json = json_decode($string, true);

        return $json;
    }
}