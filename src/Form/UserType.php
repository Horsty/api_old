<?php
/**
 * Created by PhpStorm.
 * User: cyril
 * Date: 2019-02-16
 * Time: 16:20
 */

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $required = (strtolower($builder->getMethod()) === 'post');

        $builder
            ->add('email', EmailType::class, [
                'required' => $required
            ])
            ->add('plainPassword')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection' => false,
            'method' => 'POST',
            'allow_extra_fields' => false
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }


}
