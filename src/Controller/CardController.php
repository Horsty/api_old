<?php

/**
 * Created by PhpStorm.
 * User: cyril
 * Date: 2019-02-10
 * Time: 22:23
 */

namespace App\Controller;

use App\Entity\Card;
use App\Form\CardImportType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\NamePrefix;

/**
 * Class CardController
 * @package App\Controller
 * @Rest\Route("api/cards", name="api_cards_")
 * @Rest\Version({"1.0"})
 */

class CardController extends AbstractController
{
    /**
     * Get all cards
     *
     * @Rest\Get()
     * @Rest\View(serializerGroups={"card"})
     * @param Request $request
     * @return object[]
     */
    public function getCardsAction(Request $request)
    {
        $cards = $this->getDoctrine()
            ->getRepository(Card::class)
            ->findAll();

        return $cards;
    }

    /**
     * @Rest\Get("/{id}")
     * @Rest\View(serializerGroups={"card"})
     * @param Card $card
     * @return Card
     */
    public function getCardAction(Card $card)
    {
        return $card;
    }

    /**
     * Creates an Card resource
     * @Rest\View(
     *  statusCode=Response::HTTP_CREATED
     * )
     * @Rest\Post("/")
     * @param Request $request
     * @return Card
     */
    public function postCardAction(Request $request)
    {
        $card = new Card();
        $card->setName($request->get('name'));
        $card->setDescription($request->get('description'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($card);
        $em->flush();
        // In case our POST was a success we need to return a 201 HTTP CREATED response
        return $card;
    }


    public function getForm(Card $card) {
        return $this->createForm(CardImportType::class, $card);
    }


}