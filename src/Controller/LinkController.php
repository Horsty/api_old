<?php
/**
 * Created by PhpStorm.
 * User: cyril
 * Date: 2019-02-23
 * Time: 22:09
 */

namespace App\Controller;

use App\Entity\Link;
use App\Form\LinkImportType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LinkController extends AbstractController
{
    public function getForm(Link $link) {
        return $this->createForm(LinkImportType::class, $link);
    }

}