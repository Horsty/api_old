<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryImportType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{

    public function getForm(Category $category) {
        return $this->createForm(CategoryImportType::class, $category);
    }

}
