# ./docker/php/Dockerfile
FROM php:7.2-fpm

RUN mkdir -p /usr/src/app/back
WORKDIR /usr/src/app/back

RUN docker-php-ext-install pdo_mysql
RUN pecl install apcu-5.1.8
RUN docker-php-ext-enable apcu

RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer && chmod +x /usr/local/bin/composer

WORKDIR /usr/src/app/back

# install and cache app dependencies
COPY composer.json /usr/src/app/back/composer.json
COPY composer.lock /usr/src/app/back/composer-lock.json

COPY . /usr/src/app/back

RUN PATH=$PATH:/usr/src/app/vendor/bin:bin

RUN composer install

EXPOSE 9000
